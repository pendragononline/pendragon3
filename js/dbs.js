var $ = function (id) { return document.getElementById(id); };

var noOfImages = [18, 23, 15, 17, 18, 15, 12, 25, 8]; //index numbers: 0=architecture, 1=aviation, 2=film, 3=landscape, 4=night, 5=portrait, 6=street, 7=wildlife, 8=photography main page

//Set initials
var themeUrl = "http://pendragon.online/photos/architecture"; //initial URL that the button on the photography home page navigates to
var photoNo = 1;

var slideTitles = [
    //Architecture slide titles
    ['Sainte-Chapelle Paris stained glass windows','Stained glass window light at Norwich Cathedral','Cloisters at Norwich Cathedral','The Humber Bridge','View of Manhattan from the helicopter','Looking down from The Octagon, Ely Cathedral','Looking up at the organ, Norwich Cathedral','St Edmundsbury Cathedral','Whitby Abbey looking dramatic','Eiffel Tower Detail','The most famous view of Paris?','Looking into the West Tower, Ely Cathedral','St. Pancras Station, London','Sleeping Beauty Castle at Disneyland Paris by twilight','The Octagon Lantern at Ely','View of Paddington Station, London','Eurostar terminal at St. Pancras Station, London','The Walkie Talkie and other skyscrapers in the City of London'],
    //Aviation slide titles
    ['Lightning F6 on reheat','F/A-18 Hornet climbs into the blue','Swedish SAAB Gripen top-side pass','Reds flying with the BOAC 747','Eurofighter Typhoon','Apache diving','F/A-18 Hornet climbing','Ukrainian Su-27 Flanker turn and burn','US Air Force F-35A Lightning II climb','Red Arrows inverting in the blue','Dambusters 75th Anniversary tribute','RAF Tornado GR4 final RIAT flypast','French Rafale diving into the blue','French Air Force AlphaJets take off','US Air Force F-16 Viper Demo turn and burn','Vapor climb from an F/A-18 Hornet','Harrier hovering','Flanker climbing into the skies','Flanker top-side pass','RAF Tornado GR4 take off from RAF Marham','RAF Tornado GR4 9-ship flypast','RAF Tornado GR4 victory roll','Vulcan and the Red Arrows - the final display at Fairford'],
    //Film slide titles
    ['Spring daisies in Ektar','Playing the guitar in Ektar','Portrait in Ektar','Father and son time in Portra','Thetford Forest in Portra','Waves against the rocks in Portra','Daisies in Superia X-Tra','Hydrangeas in Superia X-Tra','Prom portraits in Superia X-Tra','Prom table portraits in Superia X-Tra','Sunset drive in Velvia','Bright flowers in Velvia','Pink daisies in Velvia','Autumn colours in Velvia','Autumn colours and a blue sky in Velvia'],
    //Landscape slide titles
    ['The Needles by evening', 'St. Nectans Glen Waterfall, Cornwall', 'Lake Cluaine, Scotland', 'Eileen Donan Castle, Scotland', 'Loch Broom, Ullapool', 'Norwich skyline by sunset', 'Aardvreck Castle, Scotland', 'Smoo Cave, Scotland', 'Scottish Highlands', 'Military Road, Isle of Wight', 'Sunset at Fort Victoria, Isle of Wight', 'Rolling hills of Cornwall', 'Polzeath Beach, Cornwall', 'Wymondham Abbey in the winter mist', 'Foggy morning at Southend Pier', 'Rural sunset', 'Dolphins by the Summer Islands'],
    //Night slide titles
    ['The Kelpies by twilight', 'Pulls Ferry by twilight', 'Long exposure of Loch Broom', 'Fireworks!', 'Fye River Bridge, Norwich by twilight', 'The most magical playground?', 'Swan Lane, Norwich at Christmas', 'Speeding lights on the A11', 'Manhattan by night', 'Disneylands Hotel by twilight', 'Fireworks over the Sleeping Beauty Castle', 'Fireworks!', 'Fireworks!', 'Big Ben by twilight', 'Missed the train?', 'Wymondham Market Cross by night', 'Fireworks!', 'Quayside, Norwich, by twilight'],
    //Portrait slide titles
    ['Father and son', 'An audience', 'Prom portrait', 'Birthday', 'Harry', 'Tower Bridge', 'Mother and son', 'Prom portrait', 'Prom awards', 'Christmas Lights', 'Prom portrait', 'An audience', 'Prom portrait' ,'Prom portrait', 'Portrait'],
    //Street slide titles
    ['Norwich Market', 'Norwich Market', 'Norwich Market', 'Norwich Market', 'Norwich Market', 'Norwich Market', 'Gentlemens Walk, Norwich', 'Bedford Street, Norwich', 'By the dragon, Norwich', 'The Forum, Norwich', 'Chip stall, Norwich Market', 'Norwich Market'],
    //Wildlife slide titles
    ['Tiger', 'Snow Leopard', 'Cheetah', 'Uncle Sam!', 'Great Grey Owl in flight', 'Leopard', 'Red Panda', 'Snow Leopard face', 'Dolphin swimming', 'Butterfly', 'Butterfly' ,'Tiger', 'Red Panda' ,'Cheetah' ,'Leopard', 'Leopard', 'Dolphin swimming', 'Tiger Cub', 'Bengal Eagle Owl', 'Bengal Eagle Owl face', 'Macaws in flight', 'Tiger roar!', 'White Lion', 'Monkey behind bars', 'Peacock showing its feathers'],
    //Photography home page titles
    ['Architecture Photography', 'Aviation Photography', 'Film Photography', 'Landscape Photography', 'Night & Long Exposure Photography', 'Portait Photography', 'Street Photography', 'Wildlife Photography'],
];
var slideDescriptions = [
    //Architecture slide descriptions
    ['The stained glass windows at Sainte-Chapelle Paris are absolutely stunning - shown here in HDR and in 10mm. Definitely the most beautiful thing you will see in Paris!',
        'Light shines through the new stained glass windows at Norwich Cathedral, creating a beautiful purple light in this cathedral corridor.',
        'The cloisters provide  Norwich Cathedral with a beutiful place to cool down and walk. They are very famous.',
        'The Humber Bridge was the longest suspension bridge in the world for nearly 20 years and remains the longest in the UK. Shown here in HDR, it connects North Lincolnshire with Yorkshire.',
        'One of the most breathtaking city skylies in the world is even more dramatic when viewed from above. The Freedom Tower is the tallest building in New York and the Western world.',
        'The view from the Octagon Lantern of the naves of Ely Cathedral. This photograph was taken during the 2019 Science Fair, shown suspended in the nave is a large scale model of the moon made to celebrate 50 years of the moon landings.',
        'The organ in Norwich Cathedral sits above the pews in the nave and looks very dramatic when looked up to. The 10mm focal length of this photograph makes it look even more dramatic.',
        'Unusual view of the nave of St. Edmundsbury Cathedral in Bury St. Edmunds taken from the floor. The arches in the nave construction are made very clear.',
        'Whitby Abbey is the setting of the Dracula books and it looks very dramatic in this HDR photograph. The ruins and the shapes of the abbey give it this gothic look.',
        'The Eiffel Tower has an interesting construction - one cannot help but wonder if it inspired the design of Meccano kits!',
        'The view of the grass in front of the Eiffel Tower is possibly one of the most famous views of Paris.',
        'Ely Cathedral is a beautiful cathedral and absolutely fantastic for photography. Very few cathedrals in the UK have an interior this beautiful.',
        'St Pancras Station is one of the most picturesque stations in London - from the outside at least! Few London stations look this pretty.',
        'The Sleeping Beauty Castle just before the Illuminations show at Disneyland Paris. The show contains fireworks and water fountains and captures the spirit of Disney in 25 minutes.',
        'The Octagon Lantern is the centre-piece of Ely Cathedral. This wooden tower in the centre of the cathedral features unique and beautiful paterns and can be climbed up.',
        'Paddington Station features a famous roof - the bear is not the only famous thing to come from the station!',
        'The Eurostar Terminal was previously at London Waterloo Station, but was moved to St. Pancras over 10 years ago.',
        'The Walkie Talkie is one of the most controversial skyscrapers in London. Love or hate it, you cannot deny that it adds an interesting feature to the London skyline.'],
    //Aviation slide descriptions
    ['A Lightning F6 prepares to roar down the runway at Bruntingthorpe Aerodrome Twilight Run event. This Lightning is maintained by a group called the Lightning Preservation Society',
        'A Swiss Air Force F/A-18 puts on the afterbutners and climbs into the blue.',
        'RIAT 2019 Wings Of Swords winner, the Swedish Gripen, performs a fast top-side pass at the airshow.',
        'Few displays make an airshow crowd fall silent, but this one did. To mark 50 years of Boeing 747 service, British Airways painted one in the classic BOAC livery and flew it with the Red Arrows at RIAT 2019 in an emotional display.',
        'Sgt Jim Peteson was the RAF Typhoon Display Team pilot in 2019 and flew several very fast and powerful demonstrations over the course of RIAT 2019.',
        'British Army Apache diving to demonstrate how the Apache can attack targets on the ground.',
        'Finnish Air Force F/A-18 Hornet climbing into the blue. It may be 40 years old, but the Hornet is still a popular aircraft among several air forces and is also the star of Top Gun 2!',
        'Reheat action from a Ukrainian Air Force Su-27 Flanker as it pulls into a steep climb against a blue sky.',
        'The F-35 Lightning II is the most advanced fighter jet on the planet and has just entered service with several large air arms. Negative press aside, its performance is something to behold!',
        'The Red Arrows are one of the most famous and loved aerobatic display teams in the world. They never fail to provide an amazing and energetic display!',
        'The Lancaster bombers of 617 Sqn in the RAF performed the dangerous Dambuster raids of 1943. The Tornado GR4 (pictured below) later served in 617 Sqn and flew with the Lancaster in a tribute.',
        '2018 was the final airshow season for the RAF Tornado GR4 - seen here at Fairford during one final low, loud and fast pass over the crowdline.',
        'This beautifully painted French Air Force Rafale flew at UK airshows in the 2018 display season, pictured here pulling a stunning 9G turn at Fairford.',
        'French Air Force AlphaJets of the Patrouille-de-France national aerobatic display team taking to the skies.',
        'Reheat action from the US Air Force F-16 Viper demo team, a rare attendee to UK airshows. This was just after one of the most insane take offs I have ever witnessed!',
        'F/A-18 Hornet pulls a steep climb and as a consequence invokes the vapor trails over the wings!',
        'Spanish Navy Harrier seen hovering above Fairford the first time in almost 10 years - a special and emotional moment.',
        'Ukrainian Air Force Su-27 taking to the skies with the afterburners ignited.',
        'Top-side pass of this beautiful Cold War aircraft. Once feared by the West, the 35 year old Su-27 remains a stellar dogfighter.',
        'Tornado GR4 taking off from RAF Marham in February 2019 for one of the final times before retirement a few weeks later.',
        'Tornado GR4 farewell 9-ship formation above the skies of their home base, RAF Marham in Norfolk, in February 2019.',
        'A victory roll from a Tornado GR4 above the skies of RAF Marham in February 2019. The classic manouvere is iconic - Spitfires would often roll after a successful mission in WWII.',
        'Vulcan XH558 was the final airworthy Avro Vulcan bomber. She flew again between 2007 and 2015 and is seen here at RIAT 2015 flying with the Red Arrows for the final time in an emotional display.'],
    //Film slide descriptions
    ['Kodak Ektar 100, Pennsthorpe Nature Reserve, Fakenham, Norfolk.',
        'Kodak Ektar 100 (converted to black and white in software), at home.',
        'Kodak Ektar 100, Little Venice, London.',
        'Kodak Portra 160, Wymondham, Norfolk.',
        'Kodak Portra 160, Thetford Forest, Norfolk.',
        'Kodak Portra 160, Boscastle, Cornwall.',
        'Fuji Superia X-TRA 400, Somerleyton Hall and Gardens, Suffolk.',
        'Fuji Superia X-TRA 400, Somerleyton Hall and Gardens, Suffolk.',
        'Fuji Superia X-TRA 400, Dunston Hall, Norfolk.',
        'Fuji Superia X-TRA 400, Dunston Hall, Norfolk.',
        'Fuji Veliva 50, Wicklewood, Norfolk.',
        'Fuji Velvia 50, Wymondham, Norfolk.',
        'Fuji Velvia 50, Wymondham, Norfolk.',
        'Fuji Velvia 50, Wymondham, Norfolk.',
        'Fuji Velvia 50, Wymondham, Norfolk.'],
    //Landscape slide descriptions
    ['The Needles have an interesting history. During both World Wars they acted as a military base, protecting mainland Britain from a southern invasion. During the Cold War the site was used to test the RAF Blue Streak nuclear missile. Today, with the help of St. Catherines Lighthouse further south the lighthouse helps ships from running aground on the west side of the island.',
        'This waterfall is well-hidden and that makes it special. Visitors are able to stand in the plunge pool and it is common to take a stone from the gorge the waterfall is situated in and carve the name of a loved one into it.',
        'Loch Cluaine is one of the lesser-known Scottish lochs but is absolutely beautiful and very quiet. It is located on the A87 road and there is a hydroelectric power station located nearby. There are many hydroelectric power stations in the Scottish Highlands.',
        'The Eileen Donan Castle is possibly one of the most recognisable in the UK. It has featured in many films. Its location in among mountains and lochs makes it an ideal location for a secluded, or slightly scary, castle or home in films.',
        'Loch Broom is located directly beneath the small of Ullapool and look beautiful on this clear autumn morning. The sky is blue and the mountains in the distance look more like drawings than landforms.',
        'The distinct skyline of Norwich is silhouetted by a sunset. The cathedral spire stands taller than any other monument in the city.',
        'Hidden in the Scottish Highlands on the west coast are the ruins of Aardvreck Castle, thought to have been destroyed during a family war in which the youngest daughter had to make a pact to the devil to stay alive.',
        'Located at the top of Scotland, this impressive cave featuring a waterfall has existed for tens of thousands of years and is actively excavated to  find out how big it really is.',
        'It is hard to grow tired of the mountains in Scotland. They are all very imposing make for fantastic scenery.',
        'Military Road is one of the greatest driving roads in the UK. It goes from Ventnor to Freshwater Bay right along the western coast of the Isle of Wight - giving drivers stunning views of the beaches and cliffs in West Wight.',
        'Fort Victoria on the Isle of Wight is a little like The Needles in that it has an interesting defence history. A fortress has existed here since the reign of King Henry VIII, built to protect the mainland from a southern invasion. It was rebuilt in the Victorian era and was a military site until the early 1960s.',
        'An evening view of the rolling hills of Cornwall not far from Port Quinn in the north of the county. Few places invoke peace quite like here.',
        'The view across Polzeath Bay is absolutely breathtaking - the blue sea and high cliffs make it extremely picturesque and calm, gunny when this place is famous for surfing.',
        'A cold December evening at Wymondham Abbey. The mist surrounding the abbey was hauntingly beautiful. The abbey has stood here since 1107.',
        'Heavy sea mist shrouds Southend Pier, the longest pier in the world. Walking along the pier in this fog almost like walking up the stairs to heaven.',
        'The sun sets over a field of wheat in Norfolk. The agricultural industry is still an important part of the local economy here.',
        'The Summer Islands, west of the Scottish Highlands are home to dolphins which can sometimes be seen, weather and timing permitting. They are free and this is their natural habitat.'],
    //Night slide descriptions
    ['The Kelpies in Falkirk, Scotland, are the largest horse statues in the world and honour the working horses of the early canals. Each evening the lights cycle through different colours.',
        'Pulls Ferry is the traditional entrance to Norwich Cathedral on the River Wensum, photographed here on a rainy February evening. The building fell into disrepair and was reconstructed before the millennium.',
        'Long exposure of an evening scene in Ullapool, looking over Loch Broom and the surrounding mountains.',
        'Fireworks from the Wymondham Rugby Club - this was their final display at Tuttles Lane before they relocated to another location in the town.',
        'Looking at Fye River Bridge on a quiet April evening. On the opposite side of the bridge is The Mischief Tavern, one of the most famous pubs in Norwich.',
        'The stars shine over Happisburgh Lighthouse and the playground by it in the small hours of the morning.',
        'Icicle Christmas lights sparkling above Swan Street in Norwich. This street is part of the Norwich Lanes and has several jewellery shops and eateries on it.',
        'Light trails on the A11 on the summer solstice. A slight breeze and a cloudy night made for an epic sky.',
        'Manhattan viewed from the bottom of the Brooklyn Bridge. This was taken on a stormy September night and is one of the most famous views of New York.',
        'The Disneylands Hotel is the original hotel at Disneyland Paris and features the famous Mickey Mouse clock. The hotel looked amazing at twilight with the lights on.',
        'Sleeping Beauty Castle during the Disney Illuminations. This 25 minute show captures the spirit of Disney through the use of fireworks, water fountains, music and projections on the castle.',
        'Fireworks are difficult to photograph, I find that a long focal length and using the bulb/time mode is a good way to get the shot.',
        'I also find that going to organised firework displays results in the best photographs because the fireworks are always better than what you can buy yourself.',
        'Big Ben and the Houses of Parliament viewed from the London Eye on a December evening.',
        'Long exposure of a Cambridge-bound train departing from Wymondham Station. Whilst the photograph was being taken, commuters were walking in front of the capture, however the long exposure time did not capture their presence at all.',
        'Wymondham Market Cross in the snow with heavy snow clouds above. Wymondham is one of a hanful of towns in the UK to still have a traditional market cross. It is more elaborate than most other market crosses in that it is a building and not just a monument to mark a market square.',
        'Fireworks at Flame And Thunder 2016. The event is all about jet dragster racing - by comparison, the fireworks were very quiet but featured just as much fire!',
        'A still evening on the River Wensum in Norwich, from Fye River Bridge looking down Quayside.'],
    //Portrait slide descriptions
    ['Candid shot of the bond between father and son.',
        'Attendees at an event at Wymondham High to support skin condition charities.',
        'Low-light, large aperture portrait photograph from a school prom.',
        'The natural light from the candles on the cake illuminates the face perfectly in this subtle birthday photo.',
        'The graffiti on the wall and the sun shining makes Harry stand out a little more.',
        'Candid shot of my brother at Tower Bridge, looking over the Thames.',
        'Candid shot of the bond between mother and son.',
        'Unusual but effective composition for a group prom photograph at Dunston Hall in Norfolk.',
        'Waiting in excitement for the prom awards.',
        'Playing a family board game on Boxing Day.',
        'Portraits from the Wymondham High Academy prom.',
        'Attendees at an event at Wymondham High to support skin condition charities.',
        'Portraits from the Wymondham High Academy prom.',
        'Portraits from the Wymondham High Academy prom.',
        'Portrait taken for a project depicting image and identity.'],
    //Street slide descriptions
    ['Norwich Market is a great place for street photography. I love framing pictures around the clothing stalls.',
        'A quiet day on the market. It often gets busiest around lunch time!',
        'Bag stalls are common the market and make for interesting photo frames.',
        'The bags placed high up above the shoppers makes for an interesting scene, I feel.',
        'Capturing everyday life is something that all photographers should do. In years to come we will look at these photos and be reminded of the time they were taken.',
        'Best of British on the market. The market really encourages community and is a landmark of Norwich.',
        'Walking to work down the busy Gentlemens Walk in Norwich. This was one of the first pedestrianised streets in the UK.',
        'Bedford street is just one example of back streets in Norwich - the city is full of them. They are often quite quiet and filled with quirky shops.',
        'In 2015 the city was filled with fibre-glass dragons to help raise money for local charities. There was a trail that people could complete to see all of the dragons.',
        'A cycling event at the Forum in Norwich. The Forum was a Millennium development and has since become a meeting point in the city.',
        'Food stalls are popular on the market, including Lucys Fish and Chips.There are plenty of other food stalls for all cuisines.',
        'The fruit and veg stalls on the market are popular and look so bright and colourful. The food also looks appetising.'],
    //Wildlife slide descriptions
    ['A male tiger on top of a wooden frame.',
        'Snow leopards like to hide in the rocks in their cold habitats and watch the area around them.',
        'Cheetahs are the fastest mammals on the planet. They can accelerate faster than some supercars.',
        'The Bald Eagle is often associated with the United States of America. The Bald Eagle at Banham Zoo is also called Uncle Sam and regularly performs flying demonstrations.',
        'Great Grey Owl flying at speed right into the camera during a flying demonstration.',
        'Leopards love to sit on tree branches and watch over their area. This is how they hunt for their prey and protect their habitats.',
        'Red Pandas are exceptionally cute - and also sadly endangered. This one is taking a bite of a plant on a cool spring afternoon.',
        'This was the moment a snow leopard and I had just a pane of glass between us. I have never been able to capture a snow leopard face in quite this amount oof detail since.',
        'Dolphin swimming in the sea close to the Summer Islands just off the Scottish Highlands. This is their home where they are free to swim and live.',
        'Butterfly landing on a plant at the Butterfly World on the Isle of Wight.',
        'Butterfly landing on a bright bright plant to feed at Butterfly World, Isle of Wight.',
        'Tigers usually reside in forests. They are among the largest of the big cats, but also endangered.',
        'This Red Panda takes a bite on a plant at Banham Zoo.',
        'Cheetahs are among the smallest of the big cats, but are the fastest by miles.',
        'Leopard looking over his territory, standing on top of his branch.',
        'The leopard sits on the branch to watch for prety and look for potential hunters and danger.',
        'A dolphin swims by the Summer Islands in Scotland.',
        'A baby Tiger cub. It is always lovely to see babies in zooes - breeding programmes are a big part of many zooes as part of their conservation efforts.',
        'A Bengal Eagle Owl at the Suffolk Owl Sanctuary.',
        'Face detail of the Bengal Eagle Owl.',
        'Macaws in flight during a flying demonstration in a brids of prey demonstration. Macaws are not birds of prey, but sometimes fly during these shows.',
        'A tiger roars on the Isle of Wight Zoo. This zoo is famous for its conservation efforts and often rescues big cats from circuses in Europe that are abusing them. Their latest tigers and lions are from a Spanish circus called Circo Wonderland whose big cats were seized in 2018 by a Dutch animal protection organisation.',
        'A white lion at the Isle of the Wight Zoo. White lions are extremely rare as their colour comes about from a genetic mutation. The zoo took the decision not to breed this lion because of this and the white lion no longer resides at this zoo.',
        'A monkey in a cage at the Isle of Wight Zoo. The photograph is a little misleading because this monkey actually lives in a very large enclosure at the zoo, but its composition and the facial expressions of th monkey makes the situation seem worse than it really is.',
        'A peacock shows its feathers, presumably trying to attract a mate! I remember her not being very impressed at all.'
    ],
    //Photography home page descriptions
    ['Cathedrals, skyscrapers, cityscapes bridges and historic buildings. A wide angle lens and a stunning piece of design made a great conbination.',
        'Photographing fast jets requires anticipation, a long focal length and a fast shutter speed - and a love for noise and awesome aerobatics!',
        'Fuji Velvia, Kodak Ektar, Kodak Portra and Fuji Superia - all 35mm films that I love to use for a purer photography experience.',
        'Beautiful scenes, impressive waterfalls, mesmerising lakes and stunning sunsets. Capturing the beauty of nature.',
        'Beauty of the night - the world is a prettier place when the lights shine and the fireworks glow.',
        'Event photography is what I did the most for clients and portraits are an area of photography that I also did.',
        'Capturing everyday life and what people do, how they walk, where they go and how they live.',
        'Animals in all of their beauty. From insects to birds of prey to big cats, animals always make amazing photographs.']
];

function nextSlideButton() {
    photoNo++;
    checkPhotographyPage();
}

function previousSlideButton() {
    photoNo--;
    checkPhotographyPage();
}

function checkPhotographyPage() {
    if (pageName !== 'photography') {
        loop();
        setURL();
    }
    else {
        photographyPage();
    }
}

function setURL() {
    var url = "../images/photography/" + pageName + "/" + pageName + photoNo + ".jpg"; 
    changeSlide(url);
}

function changeSlide(url) {
    $('dragonbase-slideshow').style.backgroundImage = "url(" + url + ")";
    $('slide-title').innerHTML = slideTitles[pageIndex][photoNo-1]; 
    $('slide-description').innerHTML = slideDescriptions[pageIndex][photoNo-1];
}

function loop() {
    if (photoNo > noOfImages[pageIndex]) {
        photoNo = 1;
    }
    else if (photoNo < 1) {
        photoNo = noOfImages[pageIndex];
    }
}

function photographyPage() {
    photographyPageLoop();
    var url = "images/photography/" + themes[photoNo - 1] + "/" + themes[photoNo - 1] + "1.jpg";
    $('dragonbase-slideshow').style.backgroundImage = "url(" + url + ")";
    $('slide-title').innerHTML = slideTitles[8][photoNo-1]; 
    $('slide-description').innerHTML = slideDescriptions[8][photoNo-1];
    var currentTheme = themes[photoNo - 1].toString();
    $('theme-button').innerHTML = currentTheme.charAt(0).toUpperCase() + currentTheme.slice(1) + " Photography"; //change button text  
}

function themePageClick() { //function activated when user clicks on the theme button on the photography home page
    window.location.href = themeUrl;
}

function photographyPageLoop() {
    if (photoNo === 0) {
        photoNo = 1;
    }
    if (photoNo > 8) {
        photoNo = 1;
    }
    if (photoNo < 1) {
        photoNo = 8;
    }
}

setInterval(function () { //automate slide advance every 5 seconds  
    checkPhotographyPage();
    photoNo++;
    themeUrl = ""; //clear the URL
    themeUrl = "http://pendragon.online/photos/" + themes[photoNo - 2]; //create new URL
}, 5000);

//Get page name and index (remove need for inline JS on each gallery page)
var themes = ['architecture', 'aviation', 'film', 'landscape', 'night', 'portrait', 'street', 'wildlife'];
var pageIndex = "";
var pageName = "";
var currentPage = window.location.href;
if (currentPage.includes(themes[0])) {
    pageName = themes[0];
    pageIndex = 0;
}
if (currentPage.includes(themes[1])) {
    pageName = themes[1];
    pageIndex = 1;
}
if (currentPage.includes(themes[2])) {
    pageName = themes[2];
    pageIndex = 2;
}
if (currentPage.includes(themes[3])) {
    pageName = themes[3];
    pageIndex = 3;
}
if (currentPage.includes(themes[4])) {
    pageName = themes[4];
    pageIndex = 4;
}
if (currentPage.includes(themes[5])) {
    pageName = themes[5];
    pageIndex = 5;
}
if (currentPage.includes(themes[6])) {
    pageName = themes[6];
    pageIndex = 6;
}
if (currentPage.includes(themes[7])) {
    pageName = themes[7];
    pageIndex = 7;
}
if (currentPage.includes('photography') || currentPage.includes('error')) {
    pageName = 'photography';
    pageIndex = 8;
}

//Set initial image and heading and description text
var startURL = "";
if (pageName !== 'photography') {
    startURL = "../images/photography/" + pageName + "/" + pageName + "1.jpg";
}
else {
    startURL = "images/photography/" + themes[photoNo - 1] + "/" + themes[photoNo - 1] + "1.jpg";
    $('theme-button').innerHTML = "Architecture Photography"; 
}
$('dragonbase-slideshow').style.backgroundImage = "url(" + startURL + ")";
$('slide-title').innerHTML = slideTitles[pageIndex][0];
$('slide-description').innerHTML = slideDescriptions[pageIndex][0];



