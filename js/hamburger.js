var $ = function (id) { return document.getElementById(id); };

var open = 0;
$('hamburger-button').addEventListener('click', function () {
    if (open === 0) {
        $('nav').classList.remove('hidden');
        $('nav').classList.add('visible');  
        $('hamburger-button').innerHTML = "Close";
        $('home-button').addEventListener('click', closeMenu);
        $('portfolio-button').addEventListener('click', closeMenu);
        $('photography-button').addEventListener('click', closeMenu);
        $('workexperience-button').addEventListener('click', closeMenu);
        $('aboutme-button').addEventListener('click', closeMenu);
        open = 1;
    }
    else if (open === 1) {
        closeMenu();
    }
});

function closeMenu() {
    var mobileView = window.matchMedia("(max-width: 1024px)");
    if (mobileView.matches) {
        $('nav').classList.remove('visible');
        $('nav').classList.add('hidden');
        $('hamburger-button').innerHTML = "Menu";
        open = 0;
    }
}


