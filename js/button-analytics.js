var $ = function (id) { return document.getElementById(id); };

if (localStorage.getItem("cookies-accepted") === null) {
    $('cookie-policy').classList.add('visible'); //show the message
}
else {
    $('cookie-policy').classList.add('hidden'); //hide the message - it shows by default
}

var cookieState = localStorage.getItem("cookies-accepted"); //store value of cookie in variable

if (cookieState === 'yes') {
    collectAnalytics();
}

//No similar function to lines 7-9 for 'no analytics' because if the cookie says 'no' or 'null' then the analytics function won't be called

$('accept-cookies').addEventListener('click', function () {
    acceptCookies();
});

$('reject-cookies').addEventListener('click', function () {
    rejectCookies();
});

function acceptCookies() {
    localStorage.setItem("cookies-accepted", "yes"); //collect analytics from device (consent granted)
    var messageShow = 1;
    changeMessage(messageShow);
    collectAnalytics(); //collect analytics
    $('cookie-policy').classList.add('slideDown'); //hide message
}

function rejectCookies() {
    localStorage.setItem("cookies-accepted", "no"); //don't collect analytics from device (no consent)
    var messageShow = 0;
    changeMessage(messageShow);
    $('cookie-policy').classList.add('slideDown'); //hide message
}

function changeMessage(messageShow) {
    $('accept-cookies').classList.add('hidden');
    $('reject-cookies').classList.add('hidden');
    $('cookie-policy').classList.add('green-section');
    $('cookie-policy-title').innerHTML = "All set!";
    if (messageShow === 0) {
        $('cookie-policy-description').innerHTML = "Thanks for confirming! I won't be collecting analytics from this device.";
    }
    else if (messageShow === 1) {
        $('cookie-policy-description').innerHTML = "Thanks for confirming! Your settings help me to improve my site.";
    }       
}

function collectAnalytics() {

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-110588167-2', 'auto');
    ga('send', 'pageview');
}








