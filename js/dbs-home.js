var $ = function (id) { return document.getElementById(id); };

imageNo = 0;

var imgURL = ['images/about/jason2.jpg', 'images/home/uxpersonas.jpg', 'images/home/portfolio.png', 'images/curat/curat-1.jpg', 'images/home/accessibility.jpg', 'images/about/sdl2.jpg', 'images/photography/architecture/architecture1.jpg'];
var heroTitles = ['UX Designer', 'Putting users first', 'Design, test, code, repeat', 'Putting pieces together', 'Accessibility aware', 'Work experience', 'Photography'];
var heroDescriptions = ['I take user research and data and turn it into digital services that people enjoy using.',
    'My work starts with users. If you know your users, you can make something that they enjoy.',
    'I design, I test - and then I code.',
    'I use user personas to create information architectures, user flows and system diagrams which help to make successful products.',
    'A shocking 98% of the top one million home pages have accessibility errors. My final year university projects focused on improving experiences for the visually impaired.',
    'I have worked in various UX, design and ambassadorship roles as a student as well as volunteered in the education sector.',
    'A strong interest of mine, capturing the world with my Nikons is what I do when not designing, talking to users or coding!'];
var buttonText = ['What I Do', 'View Portolio', 'View Portfolio', 'View Portfolio', 'Read About These', 'Work Experience', 'View Photography Portfolio'];
var buttonURL = ['http://pendragon.online#ux', 'http://pendragon.online/portfolio', 'http://pendragon.online/portfolio', 'http://pendragon.online/portfolio', 'http://pendragon.online/portfolio#accessibility-projects', 'http://pendragon.online/about', 'http://pendragon.online/photography'];

function homeHeroButton() {
    window.location.href = buttonURL[imageNo];
}

function slideLoop() {
    if (imageNo === 6) {
        imageNo = 0;
    }
    if (imageNo < 0) {
        imageNo = 6;
    }
}

setInterval(function () { //automate slide advance every 5 seconds 
    slideLoop();
    imageNo++;
    $('home-hero').style.backgroundImage = "url(" + imgURL[imageNo] + ")";
    $('slide-title').innerHTML = heroTitles[imageNo];
    $('slide-description').innerHTML = heroDescriptions[imageNo];
    $('home-slide-button').innerHTML = buttonText[imageNo];
}, 7000);

$('home-hero').style.backgroundImage = "url(" + imgURL[imageNo] + ")";
$('slide-title').innerHTML = heroTitles[imageNo];
$('slide-description').innerHTML = heroDescriptions[imageNo];
$('home-slide-button').innerHTML = buttonText[imageNo];