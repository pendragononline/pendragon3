var $ = function (id) { return document.getElementById(id); };
var circles = ['discover', 'define', 'develop', 'deliver', 'evolve', 'user'];
var attentionseekers = ['jello-animation', 'wobble-animation', 'tada-animation', 'swing-animation'];
var animationfadeins = ['fade-in', 'fade-in-down', 'fade-in-left', 'fade-in-right', 'fade-in-up', 'fade-in-up-std'];
var animationentrances = ['bounce-in-right-animation', 'bounce-in-left-animation', 'bounce-in-up-animation', 'bounce-in-down-animation', 'lightspeed-in-animation', 'zoom-in-down-animation', 'zoom-in-animation', 'zoom-in-up-animation', 'zoom-in-left-animation', 'zoom-in-right-animation', 'rotate-in-animation' ];
var animationexits = ['rotate-out-animation', 'zoom-out-down-animation', 'zoom-out-up-animation', 'bounce-out-animation', 'lightspeed-out-animation'];
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

//ADD ON OPENING
$('discover').classList.add(animationentrances[getRandomInt(10)]);
$('discover-mobile').classList.add(animationentrances[getRandomInt(10)]);
$('define').classList.add(animationentrances[getRandomInt(10)]);
$('define-mobile').classList.add(animationentrances[getRandomInt(10)]);
$('develop').classList.add(animationentrances[getRandomInt(10)]);
$('develop-mobile').classList.add(animationentrances[getRandomInt(10)]);
$('deliver').classList.add(animationentrances[getRandomInt(10)]);
$('deliver-mobile').classList.add(animationentrances[getRandomInt(10)]);
$('evolve').classList.add(animationentrances[getRandomInt(10)]);
$('evolve-mobile').classList.add(animationentrances[getRandomInt(10)]);
$('user').classList.add('jello-animation');
$('user-mobile').classList.add('jello-animation');

//PORTFOLIO BUTTON LINK
$('diagram-portfolio-button').addEventListener('click', function () {
    window.location.href = "http://pendragon.online/portfolio";
});
$('diagram-portfolio-button-mobile').addEventListener('click', function () {
    window.location.href = "http://pendragon.online/portfolio";
});

var clicked = 0;

setInterval(function () { 
    if (clicked === 0) {
        $(circles[getRandomInt(5)]).classList.add(attentionseekers[getRandomInt(4)]);
        $(circles[getRandomInt(5)]+'-mobile').classList.add(attentionseekers[getRandomInt(4)]);
    }
}, 5000);

var mobileView = window.matchMedia("(max-width: 799px)"); 
    if (mobileView.matches) {
        $('discover-mobile').addEventListener('click', function () {
            $('discover-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('magglass-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('discover-explanation-mobile').classList.add(animationfadeins[getRandomInt(6)]);
            $('define-mobile').classList.add('rubberband-animation');
        });

        $('define-mobile').addEventListener('click', function () {
            $('define-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('qmark-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('define-explanation-mobile').classList.add('fade-in-animation');
            $('develop-mobile').classList.add('rubberband-animation');
        });

        $('develop-mobile').addEventListener('click', function () {
            $('develop-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('puzzle-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('develop-explanation-mobile').classList.add(animationfadeins[getRandomInt(6)]);
            $('deliver-mobile').classList.add('rubberband-animation');
        });

        $('deliver-mobile').addEventListener('click', function () {
            $('deliver-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('arrows-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('deliver-explanation-mobile').classList.add(animationfadeins[getRandomInt(6)]);
            $('evolve-mobile').classList.add('rubberband-animation');
        });

        $('evolve-mobile').addEventListener('click', function () {
            $('evolve-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('humans-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('evolve-explanation-mobile').classList.add(animationfadeins[getRandomInt(6)]);
            $('user-mobile').classList.add('rubberband-animation');
        });

        $('user-mobile').addEventListener('click', function () {
            $('user-mobile').classList.add(attentionseekers[getRandomInt(4)]);
            $('people-mobile').classList.add(animationexits[getRandomInt(4)]);
            $('user-explanation-mobile').classList.add('fade-in-animation');
            $('diagram-portfolio-button-mobile').classList.add('fade-in-animation');
        });
    }
    else {
        $('discover').addEventListener('click', function () {
            clicked = 1;
            $('discover').classList.add(attentionseekers[getRandomInt(4)]);
            $('magglass').classList.add(animationexits[getRandomInt(4)]);
            $('discover-explanation').classList.add(animationfadeins[getRandomInt(6)]);
            $('define').classList.add('rubberband-animation');
        });

        $('define').addEventListener('click', function () {
            clicked = 1;
            $('define').classList.add(attentionseekers[getRandomInt(4)]);
            $('qmark').classList.add(animationexits[getRandomInt(4)]);
            $('define-explanation').classList.add('fade-in-animation');
            $('develop').classList.add('rubberband-animation');
        });

        $('develop').addEventListener('click', function () {
            clicked = 1;
            $('develop').classList.add(attentionseekers[getRandomInt(4)]);
            $('puzzle').classList.add(animationexits[getRandomInt(4)]);
            $('develop-explanation').classList.add(animationfadeins[getRandomInt(6)]);
            $('deliver').classList.add('rubberband-animation');
        });

        $('deliver').addEventListener('click', function () {
            clicked = 1;
            $('deliver').classList.add(attentionseekers[getRandomInt(4)]);
            $('arrows').classList.add(animationexits[getRandomInt(4)]);
            $('deliver-explanation').classList.add(animationfadeins[getRandomInt(6)]);
            $('evolve').classList.add('rubberband-animation');
        });

        $('evolve').addEventListener('click', function () {
            click = 1;
            $('evolve').classList.add(attentionseekers[getRandomInt(4)]);
            $('humans').classList.add(animationexits[getRandomInt(4)]);
            $('evolve-explanation').classList.add(animationfadeins[getRandomInt(6)]);
            $('user').classList.add('rubberband-animation');
        });

        $('user').addEventListener('click', function () {
            click = 1;
            $('user').classList.add(attentionseekers[getRandomInt(4)]);
            $('people').classList.add(animationexits[getRandomInt(4)]);
            $('user-explanation').classList.add('fade-in-animation');
            $('diagram-portfolio-button').classList.add('fade-in-animation');
        });
    }











