var $ = function (id) { return document.getElementById(id); };

function acceptCookiesChange() {
    localStorage.setItem("cookies-accepted", "yes"); //collect analytics from device (consent granted)
    collectAnalytics();
    var messageShow = 1;
    changeMessage(messageShow);
}

function rejectCookiesChange() {
    localStorage.setItem("cookies-accepted", "no"); //don't collect analytics from device (no consent)
    var messageShow = 0;
    changeMessage(messageShow);
}

function changeMessage(messageShow) {
    $('cookie-message').classList.remove('hidden');
    $('cookie-policy-title').innerHTML = "All set!";
    if (messageShow === 0) {
        $('cookie-policy-description').innerHTML = "Thanks for confirming! I won't be collecting analytics from this device.";
    }
    else if (messageShow === 1) {
        $('cookie-policy-description').innerHTML = "Thanks for confirming! Analytics will be collected from this device. Your settings help improve my site.";
    }
}

function collectAnalytics() {

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-110588167-2', 'auto');
    ga('send', 'pageview');
}