var $ = function (id) { return document.getElementById(id); };
var scrollDown = function () {
    var y = window.scrollY;
    if (y >= 300) {
        $('nav').classList.add("sticky-nav");
    }
    else {
        $('nav').classList.remove("sticky-nav");
    }
};
window.addEventListener("scroll", scrollDown);